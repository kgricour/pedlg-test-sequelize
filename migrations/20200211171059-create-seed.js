'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Seeds', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            family: {
                type: Sequelize.STRING
            },
            variety: {
                type: Sequelize.STRING
            },
            description: {
                type: Sequelize.STRING
            },
            latinName: {
                type: Sequelize.STRING
            },
            sowingTips: {
                type: Sequelize.STRING
            },
            directSowingPeriod: {
                type: Sequelize.STRING
            },
            shelteredSowingPeriod: {
                type: Sequelize.STRING
            },
            harvestPeriod: {
                type: Sequelize.STRING
            },
            growingMethod: {
                type: Sequelize.STRING
            },
            exposition: {
                type: Sequelize.STRING
            },
            needWater: {
                type: Sequelize.STRING
            },
            soilNature: {
                type: Sequelize.STRING
            },
            soilQuality: {
                type: Sequelize.STRING
            },
            precocity: {
                type: Sequelize.STRING
            },
            texture: {
                type: Sequelize.STRING
            },
            color: {
                type: Sequelize.STRING
            },
            leavesSize: {
                type: Sequelize.STRING
            },
            leavesType: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Seeds');
    }
};
