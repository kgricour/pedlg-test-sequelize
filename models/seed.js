'use strict';
module.exports = (sequelize, DataTypes) => {
  const Seed = sequelize.define('Seed', {
    family: DataTypes.STRING,
    variety: DataTypes.STRING,
    description: DataTypes.STRING,
    latinName: DataTypes.STRING,
    sowingTips: DataTypes.STRING,
    directSowingPeriod: DataTypes.STRING,
    shelteredSowingPeriod: DataTypes.STRING,
    harvestPeriod: DataTypes.STRING,
    growingMethod: DataTypes.STRING,
    exposition: DataTypes.STRING,
    needWater: DataTypes.STRING,
    soilNature: DataTypes.STRING,
    soilQuality: DataTypes.STRING,
    precocity: DataTypes.STRING,
    texture: DataTypes.STRING,
    color: DataTypes.STRING,
    leavesSize: DataTypes.STRING,
    leavesType: DataTypes.STRING
  }, {});
  Seed.associate = function(models) {
      //Seed.belongsTo(models.User, { foreignKey: 'userId' })
  };
  return Seed;
};
